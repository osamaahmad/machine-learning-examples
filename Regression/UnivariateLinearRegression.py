class LinearRegressor:

    # "Normal Equation" can be used to perform linear regression as well.

    def __init__(self, xs, ys, learning_rate=0.01, regularization_parameter=1, init_w=0, init_b=0):
        self.w = init_w
        self.b = init_b
        self.xs = xs
        self.ys = ys
        self.learning_rate = learning_rate
        self.regularization_parameter = regularization_parameter
        self.m = len(xs)

    def predict(self, x):
        return x * self.w + self.b

    def cost(self):
        result = self.regularization_parameter * self.w**2
        for x, y in zip(self.xs, self.ys):
            error = self.predict(x) - y
            result += error * error

        # We divide by 2m instead of m because
        #  we'll multiply by 2 when computing
        #  the gradient, which will cancel it.
        return result / (2 * self.m)

    def batch_gradient_descent(self):

        # Since the cost function is a "Convex Function",
        #  there is only one minimum, and given that the
        #  learning rate is not too big (in which case,
        #  gradient descent will diverge), it's guaranteed
        #  that gradient descent will converge.

        db = 0
        dw = self.regularization_parameter * self.w
        for x, y in zip(self.xs, self.ys):
            error = self.predict(x) - y
            dw += error * x
            db += error
        dw /= self.m
        db /= self.m
        self.w -= self.learning_rate * dw
        self.b -= self.learning_rate * db

        # This value can will be used to determine
        #  whether we've converged or not.
        # We should add the absolute values so that
        #  the values don't cancel each other.
        return abs(dw) + abs(db)

    def train(self, epochs=100):
        for i in range(1, epochs+1):
            value = self.batch_gradient_descent()
            print(f'{i}. Value:', value)

    def train_until_convergence(self, threshold):
        i = 1
        value = threshold + 1
        while abs(value) > threshold:
            value = self.batch_gradient_descent()
            print(f'{i}. Value:', value)
            i += 1


def test_convergence(xs, ys, learning_rate, **kwargs):
    lr = LinearRegressor(xs, ys, learning_rate, **kwargs)
    lr.train_until_convergence(0.00001)
    print('Cost:', lr.cost())
    print('w:', lr.w)
    print('b:', lr.b)
    print()


def test_epochs(xs, ys, learning_rate, **kwargs):
    lr = LinearRegressor(xs, ys, learning_rate, **kwargs)
    lr.train(200)
    print('Cost:', lr.cost())
    print('w:', lr.w)
    print('b:', lr.b)
    print()


def test(xs, ys, learning_rate, **kwargs):
    test_convergence(xs, ys, learning_rate, **kwargs)


test([1, 2, 3, 4, 5], [9, 3, 2, 6, 4], 0.01, regularization_parameter=1)
# test([1, 2, 3, 4, 5], [1, 2, 3, 4, 5], 0.01)
