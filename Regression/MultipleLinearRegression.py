import numpy as np
from Utils import Scalers


class MultipleLinearRegressor:

    # "Normal Equation" can be used to perform linear regression as well.

    # Here, we're using vectorization (using numpy). This is better because
    #  performing operations such as summation or multiplication on vectors
    #  can utilize the parallel computing capabilities of the CPU and the GPU,
    #  resulting in a faster code, and also a shorter code.

    # Note that this is not "Multivariate Linear Regression", rather, this is
    #  called a "Multiple Linear Regression"

    # Feature scaling is important to make sure that all features have
    #  similar ranges, and that there exist no set of dominating features.
    #  Why do we perform feature scaling?:
    #   https://medium.com/@fbanovax/feature-scaling-e569d8fb2f75

    # When to perform feature scaling? Usually, you should perform feature
    #  scaling whenever a gradient is being calculated.

    # An effect of not performing feature scaling is that the learning rate
    #  will need to be very small, making the learning process very slow,
    #  whereas performing feature scaling allows for much bigger learning
    #  rate, making the learning process much faster.

    # Should the targets be scaled as well?:
    #  https://stats.stackexchange.com/q/111467/350213

    def __init__(self, xs, ys, learning_rate=0.01, regularization_parameter=1, init_w=None, init_b=0,
                 feature_scaler=Scalers.ZScoreScaler,
                 target_scaler=Scalers.ZScoreScaler):
        self.n = xs.shape[1]
        self.m = xs.shape[0]
        self.w = init_w if init_w is not None else np.zeros(self.n)
        self.b = init_b
        self.xs = xs
        self.ys = ys
        self.learning_rate = learning_rate
        self.regularization_parameter = regularization_parameter

        self.feature_scaler = None
        if feature_scaler is not None:
            self.feature_scaler = feature_scaler(xs)
            self.xs = self.feature_scaler.get_scaled()
            self.regularization_parameter /= (self.feature_scaler.to_divide_by**2)

        self.target_scaler = None
        if target_scaler is not None:
            self.target_scaler = target_scaler(ys)
            self.ys = self.target_scaler.get_scaled()

    def predict(self, x):
        return np.dot(x, self.w) + self.b

    def cost(self):
        result = 0
        for x, y in zip(self.xs, self.ys):
            error = self.predict(x) - y
            result += error * error
        result += np.sum(self.w * self.w * self.regularization_parameter)

        # We divide by 2m instead of m because
        #  we'll multiply by 2 when computing
        #  the gradient, which will cancel it.
        return result / (2 * self.m)

    def unscaled_cost(self):
        w, b = self.get_unscaled_coefficients()
        xs, ys = self.get_unscaled_input()

        w, b, self.w, self.b = self.w, self.b, w, b
        xs, ys, self.xs, self.ys = self.xs, self.ys, xs, ys

        cost = self.cost()

        w, b, self.w, self.b = self.w, self.b, w, b
        xs, ys, self.xs, self.ys = self.xs, self.ys, xs, ys

        return cost

    def batch_gradient_descent(self):

        # Since the cost function is a "Convex Function",
        #  there is only one minimum, and given that the
        #  learning rate is not too big (in which case,
        #  gradient descent will diverge), it's guaranteed
        #  that gradient descent will converge.

        db = 0
        dw = np.zeros(self.n)
        for x, y in zip(self.xs, self.ys):
            error = self.predict(x) - y
            dw += error * x
            db += error
        dw += self.w * self.regularization_parameter
        dw /= self.m
        db /= self.m
        self.w -= self.learning_rate * dw
        self.b -= self.learning_rate * db

        # This value can will be used to determine
        #  whether we've converged or not.
        # We should add the absolute values so that
        #  the values don't cancel each other.
        return np.sum(np.abs(dw)) + np.abs(db)

    def train(self, epochs=100):
        for i in range(1, epochs + 1):
            value = self.batch_gradient_descent()
            print(f'{i}. Value:', value)

    def train_until_convergence(self, threshold):
        i = 1
        value = threshold + 1
        while abs(value) > threshold:
            value = self.batch_gradient_descent()
            print(f'{i}. Value:', value)
            i += 1

    def get_coefficients(self):
        return self.w, self.b

    def get_unscaled_coefficients(self):

        w, b = self.get_coefficients()

        if self.feature_scaler is not None or \
           self.target_scaler  is not None:
            # Making a copy
            w, b = np.array(w), float(b)

        if self.feature_scaler is not None:
            w /= np.array(self.feature_scaler.to_divide_by)
            b -= np.sum(w * np.array(self.feature_scaler.to_add))

        if self.feature_scaler is not None:
            multiplier = self.target_scaler.to_divide_by[0]
            offset = self.target_scaler.to_add[0]
            w *= multiplier
            b *= multiplier
            b += offset

        return w, b

    def get_unscaled_input(self):
        xs = self.xs
        ys = self.ys
        if self.feature_scaler is not None:
            xs = self.feature_scaler.array
        if self.target_scaler is not None:
            ys = self.target_scaler.array
        return xs, ys


def test_convergence(xs, ys, learning_rate, threshold=0.0001, **kwargs):
    mlr = MultipleLinearRegressor(xs, ys, learning_rate, **kwargs)
    mlr.train_until_convergence(threshold)
    print('Cost:', mlr.unscaled_cost())
    w, b = mlr.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test_epochs(xs, ys, learning_rate, epochs=200, **kwargs):
    mlr = MultipleLinearRegressor(xs, ys, learning_rate, **kwargs)
    mlr.train(epochs)
    print('Cost:', mlr.unscaled_cost())
    w, b = mlr.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test(xs, ys, learning_rate, **kwargs):
    test_convergence(xs, ys, learning_rate, **kwargs)


def main():
    # test(np.array([[1], [2], [3]]), np.array([1, 2, 3]), 0.01)

    # test(np.array([[1], [2], [3]]), np.array([1, 3, 3]), 0.1)

    # features = np.array([[1, 1], [2, 4]])
    # targets = np.array([1, 2])
    # test(features, targets, 0.001)

    # Polynomial regression
    # features = np.array([[1, 1, 1], [2, 4, 8], [3, 9, 27], [4, 16, 64], [5, 25, 125], [6, 36, 216], [7, 49, 343]])
    # targets = np.array([1, 4, 9, 16, 25, 36, 49])
    # test(features, targets, 0.00007)

    # test(np.array([[1, 2, 3], [2, 3, 4], [3, 4, 5], [4, 5, 6], [5, 6, 7]]), np.array([9, 3, 2, 6, 4]), 0.01)

    features = np.array([[100, 2, 3], [-200, 3, 4], [3000, 4, 5], [-4000, 5, 6], [500, 6, 7]])
    targets = np.array([9, 3, 2, 6, 4])
    # Without feature scaling, this will take too many iterations
    #  to converge, and if we try to increase the learning rate,
    #  it'll diverge, meaning that the learning rate is too big.
    #  This happens because the ranges of values are not the same
    #  for each feature. The first feature ranges from -4000 up to
    #  3000, while the second feature ranges only from 2 up to 6.
    #  We need "Feature Scaling" here to overcome this problem.
    # Without feature scaling:
    # test(features, targets, 0.0000001, threshold=0.01, feature_scaler=None, target_scaler=None)
    # With Feature Scaling:
    test(features, targets, 0.1, threshold=0.00000000000001)


if __name__ == '__main__':
    main()
