import numpy as np
from Regression.MultipleLinearRegression import MultipleLinearRegressor
from Utils import Scalers


class PolynomialRegressor:

    # Tries to fit a polynomial curve to the input data.
    # Even though polynomials don't include roots, this class gives
    #  the ability to include roots as well, because, why not?
    # Is it guaranteed that the MSE cost function will remain
    #  convex even if roots are included?
    # You can determine whether a function is convex or not by
    #  computing its second derivative, if it's positive for all
    #  input values, then it's convex. If it's negative for all
    #  input values, then it's concave, and if it has inflection
    #  points, then it has multiple minima, in which the gradient
    #  descent can get trapped into.
    #  the second derivative for the loss function of w*sqrt(x) + b:
    #   dw = 2x, db = 2.
    #  Note how dw can be negative if x is negative, but also note
    #   that if x is negative, then sqrt(x) will involve complex
    #   numbers, is your output a complex number?
    # For more on the concavity of the cost function, take a look here:
    #  https://community.deeplearning.ai/t/including-a-square-root-in-a-polynomial-regressor/
    # This uses a MultipleLinearRegressor underneath the hood.
    #  It just computes the different powers of the input, and
    #  leave the rest to the multiple linear regressor.
    # Is it okay to use a linear regressor for this?:
    #  https://community.deeplearning.ai/t/can-a-polynomial-regressor-be-an-extension-to-a-multiple-linear-regressor/
    # After computing the coefficients, [w1, w2, ..., wn], w1 will
    #  correspond to the coefficient of the smallest power (roots
    #  included), and wn will correspond to the coefficient of the
    #  biggest power.
    # Example:
    #  roots_limit = 3 (meaning square roots and cubic roots will be computed)
    #  power_limit = 3 (meaning squares and cubes will be computed)
    #  w = [w1, w2, w3, w4, w5], b = b
    #  Curve = w1 * x^(1/3) + w2 * x^(1/2) + w3 * x + w4 * x^2 + w5 * x^3 + b

    # This is similar to feature engineering, in which you construct
    #  new features by combining some of the original features, here,
    #  the additional features are the different powers of the input.

    # Having a higher order polynomial function can lead to overfitting,
    #  in which case, the model fits to the training more that required.
    #  for example, if you have 6 points along a quadratic function, and
    #  your model is a 6th degree polynomial, your model can learn to fit
    #  exactly to these points, but will be squiggly, and not accurate at
    #  all for all other inputs.
    # To overcome this problem, some approaches can be taken. For example,
    #  adding more data points will help. You can also reduce the number
    #  of features, and keep only the most relevant features.
    #  Another approach is to lower the complexity of your model, so that
    #  it doesn't fit exactly on the training data, and be able to generalize
    #  well for other input data.
    #  We can do this by performing regularization. This class gives the
    #  ability to perform L2 regularization, in which the sum of the squared
    #  coefficients is added to the loss function, forcing the magnitude of the
    #  coefficients to be closer to 0.

    def __init__(self, xs, ys, learning_rate=0.01, regularization_parameter=1,
                 init_w=None, init_b=0, roots_limit=1, power_limit=1,
                 feature_scaler=Scalers.ZScoreScaler,
                 target_scaler=Scalers.ZScoreScaler):
        self.n = power_limit + max(0, roots_limit - 1)
        self.m = len(xs)
        self.roots_limit = roots_limit
        self.power_limit = power_limit

        constructed_xs = np.zeros((self.m, self.n))

        i = 0
        for r in reversed(range(2, roots_limit + 1)):
            constructed_xs[:, i] = xs ** (1 / r)
            i += 1

        constructed_xs[:, i] = xs
        i += 1

        for _ in range(2, power_limit + 1):
            constructed_xs[:, i] = constructed_xs[:, i - 1] * xs
            i += 1

        self.regressor = MultipleLinearRegressor(constructed_xs, ys, learning_rate, regularization_parameter,
                                                 init_w, init_b, feature_scaler, target_scaler)

    def create_row(self, x):
        result = np.zeros((self.n,))
        i = 0
        for p in reversed(range(2, self.roots_limit + 1)):
            result[i] = x ** (1 / p)
            i += 1

        result[i] = x
        i += 1

        for p in range(1, self.power_limit + 1):
            result[i] = result[i - 1] * x
            i += 1

        return result

    def predict(self, x):
        return self.regressor.predict(self.create_row(x))

    def cost(self):
        return self.regressor.cost()

    def unscaled_cost(self):
        return self.regressor.unscaled_cost()

    def batch_gradient_descent(self):
        return self.regressor.batch_gradient_descent()

    def train(self, epochs=100):
        return self.regressor.train(epochs)

    def train_until_convergence(self, threshold):
        return self.regressor.train_until_convergence(threshold)

    def get_coefficients(self):
        return self.regressor.get_coefficients()

    def get_unscaled_coefficients(self):
        return self.regressor.get_unscaled_coefficients()


def test_convergence(xs, ys, learning_rate, threshold=0.0001, **kwargs):
    pl = PolynomialRegressor(xs, ys, learning_rate, **kwargs)
    pl.train_until_convergence(threshold)
    print('Cost:', pl.unscaled_cost())
    w, b = pl.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test_epochs(xs, ys, learning_rate, epochs=200, **kwargs):
    pl = PolynomialRegressor(xs, ys, learning_rate, **kwargs)
    pl.train(epochs)
    print('Cost:', pl.unscaled_cost())
    w, b = pl.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test(xs, ys, learning_rate, **kwargs):
    test_convergence(xs, ys, learning_rate, **kwargs)


def main():
    # test(np.array([1, 2, 3, 4, 5]), np.array([9, 3, 2, 6, 4]), 0.01)
    # test(np.array([1, 2, 3, 4, 5]), np.array([1, 4, 9, 16, 25]), 0.1, power_limit=2)
    # test(np.array([1, 2, 3, 4, 5]), np.array([9, 3, 2, 6, 4]), 0.3, roots_limit=3, power_limit=3)

    # sqrt_features = np.array([2, 4, 8, 12, 13])
    # sqrt_targets = np.array([1.5, 1.5, 3, 3.5, 3.5])
    # Scaled:
    # test(sqrt_features, sqrt_targets, 0.65, threshold=0.000001, roots_limit=3, power_limit=1)
    # Unscaled:
    # test(sqrt_features, sqrt_targets, 0.008, threshold=0.00048, roots_limit=3, power_limit=1,
    #      feature_scaler=None, target_scaler=None)

    # sqrt_features = np.array([2, 4, 8, 12, 13])
    # sqrt_features = np.array([i for i in range(1000)])
    # sqrt_targets = np.sqrt(sqrt_features)
    # test(sqrt_features, sqrt_targets, 0.4, threshold=0.00002, roots_limit=3, power_limit=3)

    features = np.array([0.5, 3, 5, 12, 100])
    targets = np.array([-0.25, 6, 20, 132, 9900])
    test(features, targets, 1, threshold=0.0000000000001, power_limit=2, regularization_parameter=1)

    # Overfitting example 1
    # features = np.array([1, 7, -5, -2, 9, -1])
    # targets = np.array([0, -6, -2, 3, 2, -3])
    # test(features, targets, 0.3, threshold=0.0000001, power_limit=6, regularization_parameter=0)

    # Overfitting example 2 (points on a quadratic curve)
    # Notice how this values should fall on the quadratic
    #  curve y = x^2, rather, the resulting curve is a
    #  polynomial of the 6th degree that passes exactly on
    #  these 6 points, and gives wrong results for other points.
    features = np.array([-2, -1, -0.5, 0.5, 1, 2])
    targets = np.array([3.9, 1.21, 0.2, 0.24, 0.95, 4.11])
    # test(features, targets, 0.3, threshold=0.00000000001, power_limit=6, regularization_parameter=0)
    # With regularization:
    # This is much closer to the quadratic function.
    # test(features, targets, 0.1, threshold=0.00000000001, power_limit=6, regularization_parameter=1)


if __name__ == '__main__':
    main()
