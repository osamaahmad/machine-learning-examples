from Regression.MultipleLinearRegression import MultipleLinearRegressor
from Utils import Scalers
import numpy as np


class MultipleLogisticRegressor(MultipleLinearRegressor):

    # This model feeds a linear function to the sigmoid function.
    #  This will enable this classifier to have a linear decision
    #  boundary. If you want a classifier with a more sophisticated
    #  decision boundary, you should feed another function into the
    #  sigmoid function (a polynomial or something).
    # The only difference between a logistic regression with a linear
    #  function being fed into the sigmoid function is the prediction
    #  function and the cost function. The function for gradient
    #  descent is the same.

    # Why not use MSE for logistic regression?:
    #  https://towardsdatascience.com/why-not-mse-as-a-loss-function-for-logistic-regression-589816b5e03c

    # Note that even though this is called logistic "regression", this
    #  is a classification, and not a regression model. The name
    #  "logistic regression" is due to historic reasons only.

    # Notice that we don't scale the targets here. This is because we
    #  rely on the fact that they're either 0 or 1.
    def __init__(self, xs, ys, learning_rate=0.01, regularization_parameter=1,
                 init_w=None, init_b=0, feature_scaler=Scalers.ZScoreScaler):
        super().__init__(xs, ys, learning_rate, regularization_parameter,
                         init_w, init_b, feature_scaler, target_scaler=None)

    def predict(self, x):
        linear_result = np.dot(self.w, x) + self.b
        if linear_result > 100:
            return 1
        elif linear_result < -100:
            return 0
        sigmoid = 1 / (1 + np.exp(-linear_result))
        return sigmoid

    def cost(self):
        # For a prediction y`, and a label y, the loss for a single example is:
        #  loss = -log(`y)     if y = 1
        #       = -log(1 - y`) if y = 0
        # This can be written as (1 - y) * -log(1 - y`) + y * -log(y`)
        # The cost = the average of all losses over all examples.
        #  cost = 1/2m * sum(losses).

        result = 0
        for x, y in zip(self.xs, self.ys):
            y_hat = self.predict(x)
            sign = 1 - 2 * int(y == 0)
            result += -np.log((1 - y) + sign * y_hat)

        return result / (2 * self.m)


def test_convergence(xs, ys, learning_rate, threshold=0.0001, **kwargs):
    mlr = MultipleLogisticRegressor(xs, ys, learning_rate, **kwargs)
    mlr.train_until_convergence(threshold)
    print('Cost:', mlr.unscaled_cost())
    w, b = mlr.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test_epochs(xs, ys, learning_rate, epochs=200, **kwargs):
    mlr = MultipleLogisticRegressor(xs, ys, learning_rate, **kwargs)
    mlr.train(epochs)
    print('Cost:', mlr.unscaled_cost())
    w, b = mlr.get_unscaled_coefficients()
    print('w:', w)
    print('b:', b)
    print()


def test(xs, ys, learning_rate, **kwargs):
    test_convergence(xs, ys, learning_rate, **kwargs)


def main():

    # 1 if x[0] + x[1] > 8
    features = np.array([[1, 2], [3, 3], [12, 4], [2, 5], [3.5, 5], [5, 15]])
    targets = np.array([0, 0, 1, 0, 1, 1])
    test(features, targets, 10, threshold=0.000000001, regularization_parameter=0, feature_scaler=None)


if __name__ == '__main__':
    main()
