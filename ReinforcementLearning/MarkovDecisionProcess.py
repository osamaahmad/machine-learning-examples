# https://www.youtube.com/watch?v=9g32v7bK3Co
# https://www.youtube.com/watch?v=wIloocUqL-E

# actions_func(s): a function returning all of the possible actions from state s.

# Action(s) = list of Transition(target_state, p, r) where:
#   target_state : one of the possible states to end up at from state s by the taken action.
#   p            : probability of ending up in the state target_s
#   r            : reward of reaching state target_s from state s by the taken action.

# Q(s, a) = The expected reward obtained starting at state s, taking the action a.
#  You can see that Value(s) = sum of P(a, s) * Q(s, a) for every possible a from
#  state s where P(a) is the probability of taking the action a when at state s.

# Policy(s) = mapping from a state to an action. If the policy is optimal,
#  then the returned action is the best action to take when at state s.

# Value(s) = The expected discounted sum of rewards (utility) obtained starting at state s.

# * = optimal, for example, Policy* = optimal policy, Value* = optimal value, and so on.

# Q*(s, a) = expected utility starting at state s, and taking action a, then acting optimally.

# Policy*(s) = optimal action starting at state s
#  = argmax_a(Q*(s, a)). This means that the optimal policy at state s =
#  the action argument that maximizes the value of the optimal Q function at state s.

# Value*(s) = expected utility starting at s and acting optimally
#  = max_a(Q*(s, a)). This means that the optimal value of a state s = the
#  value of the optimal Q function evaluated at state s, with the an action a, that
#  maximizes the result

# MDP is used to model situations where outcomes of an action is non-deterministic
#  (probabilistic, or stochastic). Unlike deterministic problems, like finding shortest
#  paths for example, since an outcome of an action is non-deterministic, we can only
#  decide the best next action to take from a given state, and not the whole solution.
# In MDPs, after choosing an action, you transition to a "Chance" node, also called a
#  "Q-state", in which you can end up at different states with different probabilities.
#  For a chance node c that you end up at after taking an action a, Q(a) = Value(c).

# Cool note: In policy evaluation, since the policy is fixed (you don't have to select
#  between actions), and the transition, probability, the reward, and the discount
#  factor are all known constants, and the unknowns are only the values of each state,
#  you can solve the values of the states as a system of linear equation.

# TODO NOT TESTED!


class Transition:
    def __init__(self, target_s, p, r):
        self.target_s = target_s
        self.p = p
        self.r = r


class MDP:

    @staticmethod
    def Q(action, discount_factor, v):
        Q = 0
        for target_s, p, r in action:
            Q += p * (r + discount_factor * v[target_s])
        return Q

    @staticmethod
    def policy_evaluation(policy, discount_factor, epsilon=1e-3, max_iterations=-1):

        # Extract values from a policy

        old_v = {state: 0 for state, action in policy}
        v = {}

        # loops until convergence
        while max_iterations != 0:
            diff = 0
            for state, action in policy:
                v[state] = MDP.Q(action, discount_factor, old_v)
                diff = max(diff, abs(v[state] - old_v[state]))
            if diff <= epsilon:
                break
            max_iterations -= 1
            v, old_v = old_v, v

        # v and old_v will end up being swapped
        # thus, the most recent v = old_v
        return old_v

    @staticmethod
    def policy_extraction(v, actions_func, discount_factor):

        # Extract a policy from values

        policy = {}
        for state, _ in v:
            max_Q = float('-inf')
            max_action = None
            for action in actions_func(state):
                Q = MDP.Q(action, discount_factor, v)
                if Q > max_Q:
                    max_Q = Q
                    max_action = action
                policy[state] = max_action
        return policy

    @staticmethod
    def value_iteration(states, actions_func, discount_factor, epsilon=1e-3, max_iterations=-1):

        # This is a bottom-up dynamic programming approach.
        # At the kth iteration, we're computing the value of a state
        #  with up to only k length trajectories. Note that at the
        #  beginning, the value of a state tends to change quite often.
        #  This is because at the (k+1)th iteration, we're adding the
        #  possibility of having a trajectory with a length of k+1,
        #  which might have a bigger value than trajectories with length k.

        # Values with a trajectory length of 0 = 0.
        old_v = {state: 0 for state in states}
        v = {}

        # loops until convergence
        while max_iterations != 0:
            diff = 0
            for state in states:
                v[state] = max(MDP.Q(action, discount_factor, old_v) for action in actions_func(state))
                diff = max(diff, abs(v[state] - old_v[state]))
            if diff <= epsilon:
                break
            max_iterations -= 1
            v, old_v = old_v, v

        # v and old_v will end up being swapped
        # thus, the most recent v = old_v
        return MDP.policy_extraction(old_v, actions_func, discount_factor)

    @staticmethod
    def policy_iteration(policy, actions_func, discount_factor, epsilon=1e-3, max_iterations=-1):
        old_v = {state: 0 for state, _ in policy}
        v = {}

        while max_iterations != 0:
            diff = 0
            # loops until convergence
            # TODO replace this with a call to the policy evaluation function
            while diff > epsilon and max_iterations != 0:
                for state, action in policy:
                    v[state] = MDP.Q(action, discount_factor, old_v)
                    diff = max(diff, abs(v[state] - old_v[state]))
                max_iterations -= 1
            policy = MDP.policy_extraction(v, actions_func, discount_factor)
            v, old_v = old_v, v

        return policy


def main():
    pass


if __name__ == '__main__':
    main()
