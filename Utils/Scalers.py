import numpy as np


class Scaler:

    def __init__(self, array):
        self.array = array
        self.to_divide_by = []
        self.to_add = []
        self.result = None

    def scale(self, array):
        pass

    def get_scaled(self):

        if self.result is not None:
            return self.result

        if len(self.array.shape) == 1:
            return self.scale(self.array)

        self.result = np.zeros_like(self.array, dtype=np.float64)
        for i in range(self.array.shape[1]):
            self.result[:, i] = self.scale(self.array[:, i])

        self.to_divide_by = np.array(self.to_divide_by)
        self.to_add = np.array(self.to_add)

        return self.result


class MeanScaler(Scaler):

    def scale(self, array):
        mean = np.mean(array)
        mx = np.amax(array)
        mn = np.amin(array)
        self.to_divide_by.append(mx - mn)
        self.to_add.append(mean)
        return (array - mean) / mx - mn


class ZScoreScaler(Scaler):

    def scale(self, array):
        mean = np.mean(array)
        var = np.sum((array - mean) ** 2) / len(array)
        stdev = np.sqrt(var)
        self.to_divide_by.append(stdev)
        self.to_add.append(mean)
        return (array - mean) / stdev
