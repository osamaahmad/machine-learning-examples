import numpy as np


class MSE:

    def compute(self, y_hat, y):
        return ((y_hat - y) ** 2) / (2 * y.size)

    def derivative(self, y_hat, y):
        return y_hat - y


class LinearActivation:

    def compute(self, Z):
        return Z

    def derivative(self, Z):
        return np.ones_like(Z)


class ReLU:

    def compute(self, Z):
        return np.maximum(0, Z)

    def derivative(self, Z):
        dZ = np.ones_like(Z)
        dZ[Z < 0] = 0
        return dZ


class LeakyReLU:

    def __init__(self, c=0.5):
        self.c = c

    def compute(self, Z):
        result = np.array(Z, copy=True)
        result[Z < 0] *= self.c
        return result

    def derivative(self, Z):
        dZ = np.ones_like(Z)
        dZ[Z < 0] = self.c
        return dZ


class Sigmoid:

    def compute(self, Z):
        return 1 / (1 + np.exp(-Z))

    def derivative(self, Z):
        x = self.compute(Z)
        return x * (1 - x)


class DenseLayer:

    def __init__(self, units, activation, learning_rate=None):
        self.units = units
        self.input_size = 0
        self.activation = activation
        self.learning_rate = learning_rate
        self.W = None
        self.b = np.zeros((1, units))
        self.next = None

    def set_next(self, next):
        self.next = next

    def set_learning_rate(self, learning_rate):
        self.learning_rate = learning_rate

    def get_learning_rate(self):
        return self.learning_rate

    def init_weights(self, input_size):
        if self.W is None:
            self.input_size = input_size
            self.W = np.random.randn(self.input_size, self.units)

    def z(self, x):
        self.init_weights(x.shape[1])
        return np.dot(x, self.W) + self.b

    def compute(self, x):
        z = self.z(x)
        a = self.activation.compute(z)
        return a

    def forward_propagate(self, x):
        return self.next.forward_propagate(self.compute(x))

    def back_propagate(self, x):
        n_examples = x.shape[0]
        a = self.compute(x)
        do = self.next.back_propagate(a)
        dz = do * self.activation.derivative(a)
        db = np.sum(dz, axis=0) / n_examples
        dW = np.dot(x.T, dz) / n_examples

        self.W -= self.get_learning_rate() * dW
        self.b -= self.get_learning_rate() * db

        # This computation is not used for the first layer.
        return np.dot(dz, self.W.T)


class OutputLayer:

    def __init__(self, loss):
        self.loss = loss
        self.targets = None
        self.cost = None

    def set_targets(self, targets):
        self.targets = targets

    def forward_propagate(self, x):
        return x

    def back_propagate(self, x):
        self.cost = self.loss.compute(x, self.targets)
        return self.loss.derivative(x, self.targets)

    def get_last_cost(self):
        return self.cost


class NeuralNetwork:

    def __init__(self, layers, default_learning_rate, loss):
        self.layers = layers
        self.loss = loss

        for layer in layers:
            if layer.get_learning_rate() is None:
                layer.set_learning_rate(default_learning_rate)

        self.layers.append(OutputLayer(loss=loss))
        for i in range(1, len(layers)):
            layers[i - 1].set_next(layers[i])

    def _last_cost(self):
        return np.average(self.layers[-1].get_last_cost())

    def _fit(self, X, epochs, print_period):
        for i in range(epochs + 1):
            self.layers[0].back_propagate(X)
            if i % print_period == 0:
                print(f'Epoch {i}: Cost = {self._last_cost()}')

    def fit(self, X, Y, epochs=100000, print_period=1000):
        self.layers[-1].set_targets(Y)  # The output layer
        self._fit(X, epochs, print_period)
        self.layers[-1].set_targets(None)

    def predict(self, X):
        return self.layers[0].forward_propagate(X)

    def cost(self, X, Y):
        return self.loss.compute(self.predict(X), Y)


def main():
    nn = NeuralNetwork(
        [
            DenseLayer(10, ReLU()),
            DenseLayer(10, ReLU()),
            DenseLayer(4, ReLU()),
        ],
        1e-4,
        MSE(),
    )

    X = np.array([[1, 2, 3], [3, 4, 5], [3, 3, 3]])
    Y = np.array([[1, 2, 3, 4], [4, 5, 6, 7], [2, 3, 1, 1]])

    nn.fit(X, Y, 100000)

    print(nn.predict(X))
    print()
    print(nn.predict(np.array([[1, 1, 1], [2, 3, 2], [2, 2, 2]])))


if __name__ == '__main__':
    main()

