import numpy as np
import matplotlib.pyplot as plt


def transform_examples(examples, transformers):
    examples_count = examples.shape[0]
    if transformers is not None:
        for i in range(examples_count):
            if transformers[i] is not None:
                examples[i] = transformers[i](examples[i])
    return examples


class DensityEstimator:

    def __init__(self, examples, transformers=None):
        # Here, we assume that every dimension of the examples is
        #  independent. In practice, it makes a little difference
        #  if the dimensions are dependent. Furthermore, here, each
        #  dimension is modeled as a normal (gaussian) distribution.
        #  If there exists dimensions that're not normally distributed,
        #  you can try to transform it using one of the transformers
        #  classes. Some possible transformations are:
        #   - x = log(x)
        #   - x = log(x + c) where c is a constant
        #   - x = sqrt(x)
        #   and so on
        # Why normal distribution?:
        #  https://community.deeplearning.ai/t/anomaly-detection-with-different-probability-distributions
        examples = transform_examples(examples, transformers)
        self.mean = np.mean(examples, axis=0)
        self.std = np.std(examples, axis=0)

    def pdf(self, x):
        return (1.0 / (self.std * np.sqrt(2 * np.pi))) * np.exp(-0.5 * ((x - self.mean) / self.std) ** 2)

    def estimate(self, x):
        return np.product(self.pdf(x), axis=1)


class AnomalyDetector:

    def __init__(self, examples, epsilon, transformers=None):
        self.estimator = DensityEstimator(examples, transformers)
        self.epsilon = epsilon

    def is_anomaly(self, x):
        return self.estimator.estimate(x) < self.epsilon


def test(epsilon):

    examples = np.random.multivariate_normal([10, 20], [[25, 0], [0, 36]], 50)
    test_x = np.array([[30, 12], [15, 20], [-20, 40], [45, 45], [10, -10], [10, 20], [13, 13]])
    test_y = np.array([1, 0, 1, 1, 1, 0, 0])  # 1 if anomaly
    transformers = None
    detector = AnomalyDetector(examples, epsilon, transformers)
    y = detector.is_anomaly(test_x)

    # color index of correctly detected points = 0
    # color index of wrongly detected points = 1
    # color index of the original example points = 2
    y_colors = np.logical_xor(test_y, y)

    colors_letters = ['g', 'r', 'b']
    colors = [2] * len(examples)
    colors.extend(y_colors)
    colors = [colors_letters[i] for i in colors]

    points = np.concatenate((examples, test_x))
    x = points[:, 0]
    y = points[:, 1]

    plt.scatter(x, y, color=colors)
    plt.show()


test(0.001)
