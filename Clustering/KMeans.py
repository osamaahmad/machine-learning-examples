import numpy as np
import matplotlib.pyplot as plt


class NaiveNearestPointFinder:

    def __init__(self, centroids):
        self.centroids = centroids

    def find_closest(self, point):
        # O(n) per point. Ideally, you would want to use something
        #  like k-d tree for finding the closest centroid
        distances_squared = np.sum((self.centroids - point) ** 2, axis=1)
        return np.argmin(distances_squared)


def random_subset(array, count):
    indices = np.random.choice(len(array), size=count, replace=False)
    return array[indices]


class KMeans:

    def __init__(self, k, examples, nearest_point_finder=NaiveNearestPointFinder, iterations_limit=10000):
        self.k = k
        self.examples_count = examples.shape[0]
        self.dimensions = examples.shape[1]
        self.examples = examples
        self.nearest_point_finder = nearest_point_finder
        self.iterations_limit = iterations_limit
        self.min_loss = float('inf')
        self.clusters_indices = np.zeros((self.examples_count,), dtype=np.int32)
        self.compute_again()

    def loss(self, centroids, clusters_indices):
        result = 0
        for i in range(self.examples_count):
            index = clusters_indices[i]
            example = self.examples[i]
            centroid = centroids[index]
            result += np.sum((example - centroid) ** 2)
        return result

    def compute_again(self):

        # K Means has an optimization objective (a loss function). It
        #  tries to minimize the distance between each point and the
        #  centroid of its cluster.
        # This is done by 2 repeated steps until convergence (or until
        #  some number of iteration):
        #  1 - Change the cluster assignments of the points, while holding
        #      the position of the centroids constant, such that the resulting
        #      assignment has the minimum possible cost with such centroids.
        #      This is done by assigning each point to the cluster which has the
        #      closest centroid to it.
        #  2 - Change the position of the centroids while holding the cluster
        #      assignments constant such that the resulting positions of the
        #      centroids has the minimum possible cost with such assignments.
        #      This is done by changing the centroids to be at the mean of its
        #      cluster.
        # Each iteration minimizes (or stays the same) the cost function, thus,
        #  it's guaranteed that this algorithm will converge.
        # Even though it's guaranteed to converge, it might take a very long
        #  time to converge, thus, you can also specify a maximum number of
        #  iterations.
        # Notice that different initializations of the centroids can lead to
        #  different results. Thus, calling this function multiple times is
        #  a good idea.
        # When the resulting clustering is not optimal, this means that the
        #  algorithm has stuck in a local minimum, and not the global minimum.
        # You might choose the "k" value using the "elbow method".

        # initialize the centroids with a random k input points.
        centroids = random_subset(self.examples, self.k)
        clusters_indices = np.zeros((self.examples_count,), dtype=np.int32)
        prev_indices = np.zeros((self.examples_count,), dtype=np.int32)

        for _ in range(self.iterations_limit):

            # Assigning the centroid indices while holding
            #  the positions of the centroids constant.
            prev_indices, clusters_indices = clusters_indices, prev_indices
            finder = self.nearest_point_finder(centroids)
            for i in range(self.examples_count):
                clusters_indices[i] = finder.find_closest(self.examples[i])

            # Convergence
            if (clusters_indices == prev_indices).all():
                break

            # Changing the positions of the centroids
            #  while holding the assignments constant.
            centroids = np.zeros((self.k, self.dimensions))
            clusters_count = np.zeros((self.k, 1))
            for i in range(self.examples_count):
                index = clusters_indices[i]
                centroids[index] += self.examples[i]
                clusters_count[index] += 1
            centroids /= clusters_count

            current_loss = self.loss(centroids, clusters_indices)
            if current_loss < self.min_loss:
                self.min_loss = current_loss
                self.clusters_indices = clusters_indices


def cluster_random(k, examples_count, dimensions, trials=100):
    examples = np.random.rand(examples_count, dimensions) * 100
    kmeans = KMeans(k, examples)
    # One trial is done when the object is created
    for _ in range(trials - 1):
        kmeans.compute_again()
    return examples, kmeans.clusters_indices


def draw_2d_clusters(examples, clusters_indices):
    x = examples[:, 0]
    y = examples[:, 1]
    colors = clusters_indices
    plt.scatter(x, y, c=colors)
    plt.show()


if __name__ == '__main__':
    examples, clusters_indices = cluster_random(5, 100, 2)
    draw_2d_clusters(examples, clusters_indices)
